
CONTENTS OF THIS FILE
---------------------

 * Introduction


INTRODUCTION
------------

Current Maintainer: Kai Johnson <kaidjohnson@gmail.com>

Misson Control is a Drupal module that aims to focus the user's content
management workflow into a simplified, consistent UX.
